#!/usr/bin/env bash

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.
#
# If you have user-specific configurations you would like
# to apply, you may also create user-customizations.sh,
# which will be run after this script.

# Install PostGIS extension (PostgreSQL)
# Let's check if postgis is already installed...
dpkg -s postgis >/dev/null 2>&1
# ... if it is, then it assumed that so is postgresql-10-postgis package and that
# the postgis extension was already created in the database -> bail early
if [[ $? -ne 0 ]]; then
	# Taken from https://elliotwms.github.io/2016/homestead-postgis/
	sudo apt-get update
	sudo apt-get install -y \
		postgis \
		postgresql-10-postgis-2.4

	# Assign the correct values to these variables
	db_user=homestead
	db_pass=secret
	db_name=homestead

	# Use PGPASSWORD to login without password prompt
	# https://www.postgresql.org/docs/9.1/static/libpq-envars.html
	PGPASSWORD=secret psql \
		-U $db_user \
		-h localhost \
		-c "CREATE EXTENSION postgis;" \
		$db_name
fi
