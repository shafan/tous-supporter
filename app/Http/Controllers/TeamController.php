<?php

namespace App\Http\Controllers;

use App\Team;
use App\Ligue;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTeam;
use App\Http\Resources\Team as TeamResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Builder;

class TeamController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return App\Http\Resources\Team
     */
    public function index(Request $request)
    {
        //Get all task
        $teamRequest = Team::with(['ligue', 'ligue.federation']);
        if ($request->has('filter')) {
            $filter = $request->filter;
            $teamRequest->whereHas('ligue.federation', function (Builder $query) use ($filter) {
                $query->whereIn('id', $filter);
            });
            //$teamRequest->with(['ligue', 'ligue.federation' => function($query) use ($filter) {
            //    $query->whereIn('id', $filter);
            //}]);
        }

        if ($request->has('lat') && $request->has('lon')) {
            $lat = $request->lat;
            $lon = $request->lon;
            $teamRequest->location($lon, $lat);
        } else {
            $teamRequest->orderBy('name');
        }


        $teams = $teamRequest->paginate(5);

        // Return a collection of $task with pagination
        return TeamResource::collection($teams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StoreTeam
     * @return App\Http\Resources\Team
     */
    public function store(StoreTeam $request)
    {
        // The incoming request is valid...

        // Retrieve the validated input data...
        $validated = $request->validated();

        //LOGO
        if ($request->hasFile('logo')) {
            $filename = str_slug($validated['name']);
            $extension = $request->file('logo')->getClientOriginalExtension();
            $path = $request->file('logo')->storeAs('teams', $filename.'.'.$extension);
            $validated['logo_path'] = $path ;
            unset($validated['logo']);
        }

        $team = Team::create($validated);

        return new TeamResource($team);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return App\Http\Resources\Team
     */
    public function show(Team $team)
    {
        $team->load('ligue', 'ligue.federation');
        return new TeamResource($team);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\StoreTeam
     * @param  \App\Team  $team
     * @return App\Http\Resources\Team
     */
    public function update(StoreTeam $request, Team $team)
    {
        // The incoming request is valid...

        // Retrieve the validated input data...
        $validated = $request->validated();

        if ((
                ($request->has('remove_logo') && $request->get('remove_logo') == "1")
                || $request->hasFile('logo')
            )
            && $team->logo_path != $team::DEFAULT_LOGO) {
            Storage::delete($team->logo_path);
            $validated['logo_path'] = $team::DEFAULT_LOGO;
            unset($validated['remove_logo']);
        }

        if ($request->hasFile('logo')) {
            $filename = str_slug($validated['name']).'_'.str_random(4);
            $extension = $request->file('logo')->getClientOriginalExtension();
            $path = $request->file('logo')->storeAs('teams', $filename.'.'.$extension);
            $validated['logo_path'] = $path ;
            unset($validated['logo']);
        }
        $team->fill($validated);

        $team->save();

        $team->load('ligue', 'ligue.federation');

        return new TeamResource($team);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        $team->delete();

        return response()->json(null, 204);
    }
}
