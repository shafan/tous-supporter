<?php

namespace App\Http\Controllers;

use App\Federation;
use Illuminate\Http\Request;
use App\Http\Requests\StoreFederation;
use App\Http\Resources\Federation as FederationResource;
use Illuminate\Support\Facades\Storage;

class FederationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except(['index', 'show']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Federation::select();

        if ($request->ligue) {
            $query->with('ligues');
        }

        if ($request->paginate) {
            $query->paginate(5);
        }

        $federations = $query->get();

        // Return a collection of $task with pagination
        return FederationResource::collection($federations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StoreFederation  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFederation $request)
    {
        // The incoming request is valid...

        // Retrieve the validated input data...
        $validated = $request->validated();

        if ($request->hasFile('logo')) {
            $filename = str_slug($validated['name']);
            $extension = $request->file('logo')->getClientOriginalExtension();
            $path = $request->file('logo')->storeAs('federations', $filename.'.'.$extension);
            $validated['logo_path'] = $path ;
            unset($validated['logo']);
        }

        if ($request->hasFile('logo_black')) {
            $filename = str_slug($validated['name']);
            $extension = $request->file('logo_black')->getClientOriginalExtension();
            $path = $request->file('logo_black')->storeAs('federations', $filename.'_black.'.$extension);
            $validated['logo_black_path'] = $path ;
            unset($validated['logo_black']);
        }

        $federation = Federation::create($validated);

        return new FederationResource($federation);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Federation  $federation
     * @return \Illuminate\Http\Response
     */
    public function show(Federation $federation)
    {
        return new FederationResource($federation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\StoreFederation  $request
     * @param  \App\Federation  $federation
     * @return \Illuminate\Http\Response
     */
    public function update(StoreFederation $request, Federation $federation)
    {
        $validated = $request->validated();

        if ((
                ($request->has('remove_logo') && $request->get('remove_logo') == "1")
                || $request->hasFile('logo')
            )
            && $federation->logo_path != $federation::DEFAULT_LOGO) {
            Storage::delete($federation->logo_path);
            $validated['logo_path'] = $federation::DEFAULT_LOGO;
            unset($validated['remove_logo']);
        }

        if ($request->hasFile('logo')) {
            $filename = str_slug($validated['name']);
            $extension = $request->file('logo')->getClientOriginalExtension();
            $path = $request->file('logo')->storeAs('federations', $filename.'.'.$extension);
            $validated['logo_path'] = $path ;
            unset($validated['logo']);
        }

        if ((
                ($request->has('remove_logo_black') && $request->get('remove_logo_black') == "1")
                || $request->hasFile('logo_black')
            )
            && $federation->logo_black_path != $federation::DEFAULT_BLACK_LOGO) {
            Storage::delete($federation->logo_black_path);
            $validated['logo_black_path'] = $federation::DEFAULT_BLACK_LOGO;
            unset($validated['remove_logo_black']);
        }

        if ($request->hasFile('logo_black')) {
            $filename = str_slug($validated['name']);
            $extension = $request->file('logo_black')->getClientOriginalExtension();
            $path = $request->file('logo_black')->storeAs('federations', $filename.'_black.'.$extension);
            $validated['logo_black_path'] = $path ;
            unset($validated['logo_black']);
        }
        $federation->fill($validated);
        $federation->save();

        return new FederationResource($federation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Federation  $federation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Federation $federation)
    {
        $federation->delete();

        return response('Federation '.$federation->name.' deleted', 200);
    }
}
