<?php

namespace App\Http\Controllers;

use App\Ligue;
use App\Federation;
use Illuminate\Http\Request;
use App\Http\Requests\StoreLigue;
use App\Http\Resources\Ligue as LigueResource;
use Illuminate\Support\Facades\Storage;

class LigueController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ligues = Ligue::with('federation')->paginate(5);

        // Return a collection of $task with pagination
        return LigueResource::collection($ligues);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param App\Http\Request\StoreLigue  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLigue $request)
    {
        // The incoming request is valid...

        // Retrieve the validated input data...
        $validated = $request->validated();

        if ($request->hasFile('logo')) {
            $filename = str_slug($validated['name']);
            $extension = $request->file('logo')->getClientOriginalExtension();
            $path = $request->file('logo')->storeAs('ligues', $filename.'.'.$extension);
            $validated['logo_path'] = $path ;
            unset($validated['logo']);
        }

        $ligue = Ligue::create($validated);

        return new LigueResource($ligue);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ligue  $ligue
     * @return \Illuminate\Http\Response
     */
    public function show(Ligue $ligue)
    {
        return new LigueResource($ligue);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param App\Http\Request\StoreLigue  $request
     * @param  \App\Ligue  $ligue
     * @return \Illuminate\Http\Response
     */
    public function update(StoreLigue $request, Ligue $ligue)
    {
        // The incoming request is valid...

        // Retrieve the validated input data...
        $validated = $request->validated();

        if ((
                ($request->has('remove_logo') && $request->get('remove_logo') == "1")
                || $request->hasFile('logo')
            )
            && $ligue->logo_path != $ligue::DEFAULT_LOGO) {
            Storage::delete($ligue->logo_path);
            $validated['logo_path'] = $ligue::DEFAULT_LOGO;
            unset($validated['remove_logo']);
        }

        if ($request->hasFile('logo')) {
            $filename = str_slug($validated['name']);
            $extension = $request->file('logo')->getClientOriginalExtension();
            $path = $request->file('logo')->storeAs('ligues', $filename.'.'.$extension);
            $validated['logo_path'] = $path ;
            unset($validated['logo']);
        }
        $ligue->fill($validated);
        $ligue->save();

        return new LigueResource($ligue);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ligue  $ligue
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ligue $ligue)
    {
        $ligue->delete();

        return response('Ligue '.$ligue->name.' deleted', 200);
    }
}
