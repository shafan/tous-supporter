<?php

namespace App\Http\Resources;

use App\Http\Resources\Federation as FederationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Ligue extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'abbreviation' => $this->abbreviation,
            'url' => $this->url,
            'sexe' => $this->sexe,
            'level' => $this->level,
            'logo_path' => $this->logo_path,
            'federation' => new FederationResource($this->whenLoaded('federation')),
        ];
    }
}
