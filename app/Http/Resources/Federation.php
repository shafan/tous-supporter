<?php

namespace App\Http\Resources;

use App\Http\Resources\Ligue as LigueResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Federation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'logo_path' => $this->logo_path,
            'logo_black_path' => $this->logo_black_path,
            'ligues' => LigueResource::collection($this->whenLoaded('ligues')),
        ];
    }
}
