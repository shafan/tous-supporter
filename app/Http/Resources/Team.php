<?php

namespace App\Http\Resources;

use App\Http\Resources\Ligue as LigueResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class Team extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'fullename' => $this->fullname,
            'abbreviation' => $this->abbreviation,
            'ville' => $this->ville,
            'url' => $this->url,
            'lat' => $this->lat,
            'lon' => $this->lon,
            'logo_path' => $this->logo_path,
            'distance' => $this->distance,
            'ligue' => new LigueResource($this->whenLoaded('ligue')),
            'bg_color' => $this->bg_color,
            'color' => $this->color,
            'validated' => $this->validated,
        ];
    }
}
