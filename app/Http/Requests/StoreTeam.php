<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTeam extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @todo autoriser seulement l'administrateur
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validation = [
            'abbreviation'  =>  'nullable|string|max:255',
            'ville'  =>  'nullable|string|max:255',
            'url'   =>  'nullable|url',
            'logo'  =>  'nullable|image|dimensions:min_width=100,min_height=100,max_width=800,max_height=800|
                        mimes:jpeg,png',
            'lat' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'lon' => ['required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'validated'   =>  'required|boolean',
            'bg_color' => ['regex:/^#(\d|a|b|c|d|e|f){6}$/i'],
            'color' => ['regex:/^#(\d|a|b|c|d|e|f){6}$/i'],
        ];
        switch ($this->method()) {
            case 'DELETE':
                break;
            case 'POST':
                $validation['name'] = 'required|string|max:255|unique:teams,name';
                $validation['fullname'] = 'nullable|string|max:255|unique:teams,fullname';
                $validation['ligue_id']  =  'required|integer|exists:ligues,id';
                break;
            case 'PATCH':
                $team = $this->route('team');
                $validation['name'] = 'required|string|max:255|unique:teams,name,'.$team->id;
                $validation['fullname'] = 'nullable|string|max:255|unique:teams,fullname,'.$team->id;
                $validation['ligue_id']  =  'nullable|integer|exists:ligues,id';
                break;
            default:
                break;
        }

        return $validation;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'logo.dimensions'  => __('The logo format : min=100x100, max=800x800, ration:1/1'),
        ];
    }
}
