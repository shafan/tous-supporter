<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFederation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @todo autoriser seulement l'administrateur
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = 'unique:federations,name';

        if ($federation = $this->route('federation')) {
            $unique = 'unique:federations,name,'.$federation->id;
        }

        return [
            'name'  =>  'required|string|max:255|'.$unique,
            'abbreviation'  =>  'nullable|string|max:255',
            'url'   =>  'nullable|url',
            'logo'  =>  'nullable|image|dimensions:min_width=100,min_height=100,max_width=800,max_height=800|
                        mimes:jpeg,png',
            'logo_black'  =>  'nullable|image|dimensions:min_width=100,min_height=100,max_width=800,max_height=800|
                        mimes:jpeg,png',
            'validated'   =>  'required|boolean',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'logo.dimensions'  => __('The logo format : min=100x100, max=800x800, ration:1/1'),
        ];
    }
}
