<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ligue extends Model
{
    const DEFAULT_LOGO = "default_ligue.png";

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'logo_path' => self::DEFAULT_LOGO,
    ];

    /**
     * Get the teams for the ligue.
     */
    public function teams()
    {
        return $this->hasMany('App\Team');
    }

    /**
     * Get the federation that owns the ligue.
     */
    public function federation()
    {
        return $this->belongsTo('App\Federation');
    }
}
