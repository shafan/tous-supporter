<?php

namespace App;

use Illuminate\Support\Facades\DB;
use App\Events\TeamCreating;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    const DEFAULT_LOGO = "default_team.png";
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['position'];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'logo_path' => self::DEFAULT_LOGO,
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            if ($model->lon && $model->lat) {
                $model->position = 'SRID=4326;POINT('.$model->lon.' '.$model->lat.')';
            }
        });
    }

    /**
     * Get the ligue that owns the team.
     */
    public function ligue()
    {
        return $this->belongsTo('App\Ligue');
    }

    /**
     * The users that belong to the team.
     */
    public function users()
    {
        return $this->belongsToMany('App\User')
                    ->as('subscription')
                    ->withTimestamps();
    }

    /**
     * Scope a query with lat - lng.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLocation($query, $lon, $lat)
    {
        //return $query->when(request('lat') && request('lng'), function ($q) {
        //    $q->addSelect('ST_Distance(POINT('.$lat.' '.$lng.'),location) as distance');
        //    return $q->orderBy('distance', 'desc');
        //});
        $query->selectRaw('*,ST_Distance(\'SRID=4326;POINT('.$lon.' '.$lat.')\',position) as distance');
        $query->whereNotNull('lon');
        $query->whereNotNull('lat');
        return $query->orderBy('distance', 'asc');
    }
}
