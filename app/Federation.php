<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Federation extends Model
{
    const DEFAULT_LOGO = "default_federation.png";
    const DEFAULT_BLACK_LOGO = "default_federation.png";

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'logo_path' => self::DEFAULT_LOGO,
        'logo_black_path' => self::DEFAULT_BLACK_LOGO,
    ];

    /**
     * Get the ligues for the federation.
     */
    public function ligues()
    {
        return $this->hasMany('App\Ligue');
    }

    /**
     * Get all of the teams for the federation.
     */
    public function teams()
    {
        return $this->hasManyThrough('App\Team', 'App\Ligue');
    }
}
