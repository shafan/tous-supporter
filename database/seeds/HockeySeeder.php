<?php

class HockeySeeder extends SportSeeder
{
    protected $infos_path = [
        'json_path' => 'hockey/teams.json',
        'logo_path' => 'hockey/logo/',
    ];

    protected $federation_infos = [
        "name" => "Fédération Francaise de Hockey sur Glace",
        "abbreviation" => "FFHG",
        "url" => "https://www.hockeyfrance.com/",
        "logo" => "hockey/ffhg.png",
        "logo_black" => "hockey/ffhg_black.png",
    ];

    protected $ligues = [
        "magnus" => [
            "infos" => [
                "name" => "Synerglace Ligue Magnus",
                "abbreviation" => "",
                "url" => "https://www.liguemagnus.com/",
                "logo" => "hockey/magnus.png",
                "sexe" => "M",
                "level" => 1,
            ],
            "teams" => [],
        ]
    ];
}
