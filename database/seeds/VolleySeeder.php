<?php

class VolleySeeder extends SportSeeder
{
    protected $infos_path = [
        'json_path' => 'volley/teams.json',
        'logo_path' => 'volley/logo/',
    ];
    
    protected $federation_infos = [
        "name" => "Fédération Française de Volley-Ball",
        "abbreviation" => "FFVB",
        "url" => "http://www.ffvb.org",
        "logo" => "volley/ffvb.png",
        "logo_black" => "volley/ffvb_black.png",
    ];
    
    protected $ligues = [
        "ligue_a" => [
            "infos" => [
                "name" => "Ligue A Masculine",
                "abbreviation" => "LAM",
                "url" => "http://www.lnv.fr/9/lam.html",
                "logo" => "volley/lam.png",
                "sexe" => "M",
                "level" => 1,

            ],
            "teams" => [],
        ],
        "ligue_b" => [
            "infos" => [
                "name" => "Ligue B Masculine",
                "abbreviation" => "LBM",
                "url" => "http://www.lnv.fr/11/lbm.html",
                "logo" => "volley/lbm.png",
                "sexe" => "M",
                "level" => 2,

            ],
            "teams" => [],
        ],
        "ligue_a_f" => [
            "infos" => [
                "name" => "Ligue A Féminine",
                "abbreviation" => "LAF",
                "url" => "http://www.lnv.fr/10/laf.html",
                "logo" => "volley/laf.png",
                "sexe" => "F",
                "level" => 1,

            ],
            "teams" => [],
        ],
    ];
}
