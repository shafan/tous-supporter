<?php

class FootSeeder extends SportSeeder
{
    protected $infos_path = [
        'json_path' => 'foot/teams.json',
        'logo_path' => 'foot/logo/',
    ];

    protected $federation_infos = [
        "name" => "Fédération Française de Football",
        "abbreviation" => "FFF",
        "url" => "https://www.fff.fr",
        "logo" => "foot/fff.png",
        "logo_black" => "foot/fff_black.png",
    ];

    protected $ligues = [
        "ligue1" => [
            "infos" => [
                "name" => "Ligue 1 Conforama",
                "abbreviation" => "Ligue 1",
                "url" => "https://www.lfp.fr/ligue1",
                "logo" => "foot/ligue1.png",
                "sexe" => "M",
                "level" => 1,

            ],
            "teams" => [],
        ],
        "ligue2" => [
            "infos" => [
                "name" => "Domino's Ligue 2",
                "abbreviation" => "Ligue 2",
                "url" => "https://www.lfp.fr/ligue2",
                "logo" => "foot/ligue2.png",
                "sexe" => "M",
                "level" => 2,

            ],
            "teams" => [],
        ]
    ];
}
