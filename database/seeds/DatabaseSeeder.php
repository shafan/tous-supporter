<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BasketSeeder::class);
        $this->call(FootSeeder::class);
        $this->call(HandballSeeder::class);
        $this->call(RugbySeeder::class);
        $this->call(VolleySeeder::class);
        $this->call(HockeySeeder::class);

        $this->call(UsersTableSeeder::class);
    }
}
