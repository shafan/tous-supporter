<?php

use App\Team;
use App\Ligue;
use App\Federation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class SportSeeder extends Seeder
{
    protected $federation;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createFederation();

        $this->createLigues();

        $this->createTeams();
    }

    protected function createFederation(){
        $federation = $this->federation_infos;

        $logo = new File('storage/app/data/'.$federation['logo']);
        $filename = str_slug($federation['name']);
        $extension = $logo->extension();
        $path = Storage::putFileAs('federations', $logo, $filename.'.'.$extension);
        unset($federation['logo']);

        $federation['logo_path'] = $path;

        $logo_black = new File('storage/app/data/'.$federation['logo_black']);
        $path = Storage::putFileAs('federations', $logo_black, $filename.'_black.'.$extension);
        unset($federation['logo_black']);

        $federation['logo_black_path'] = $path;

        $this->federation = Federation::create($federation);
    }

    protected function createLigues(){
        foreach($this->ligues as $ligue){
            $ligue = $ligue['infos'];
            $logo = new File('storage/app/data/'.$ligue['logo']);
            $filename = str_slug($ligue['name']);
            $extension = $logo->extension();
            $path = Storage::putFileAs('ligues', $logo, $filename.'.'.$extension);
            unset($ligue['logo']);

            $ligue['logo_path'] = $path;
            $this->federation->ligues()->create($ligue);
        }
    }

    protected function createTeams(){
        $json = Storage::disk('local')->get('data/'.$this->infos_path['json_path']);
        $data= json_decode($json);

        foreach($data->features as $feature){
            $team = [];
            $team['name'] = $feature->properties->nom;
            $team['fullname'] = $feature->properties->nomComplet;
            $team['abbreviation'] = $feature->properties->surnom;
    
            if(!empty($feature->properties->url)){
                $team['url'] = $feature->properties->url;
            }

            if(!empty($feature->properties->logo)){
                $logo = new File('storage/app/data/'.$this->infos_path['logo_path'].$feature->properties->logo);
                $filename = str_slug($team['name']);
                $extension = $logo->extension();
                if(in_array($extension, array("jpg", "png", "JPG", "PNG", "JPEG", "jpeg"))){
                    $path = Storage::putFileAs('teams', $logo, $filename.'.'.$extension);
                    $team['logo_path'] = $path;
                }
            }

            if(!empty($feature->geometry->coordinates)){
                $team['lon'] = $feature->geometry->coordinates[0];
                $team['lat'] = $feature->geometry->coordinates[1];
            }

            $this->ligues[$feature->properties->championnat_id]['teams'][] = $team;
        }

        foreach($this->ligues as $ligue){
            $ligue_object = $this->federation->ligues()->where('name', $ligue['infos']['name'])->first();
            $ligue_object->teams()->createMany($ligue['teams']); 
        };
    }
}
