<?php

class HandballSeeder extends SportSeeder
{
    protected $infos_path = [
        'json_path' => 'handball/teams.json',
        'logo_path' => 'handball/logo/',
    ];

    protected $federation_infos = [
        "name" => "Fédération Française de Handball",
        "abbreviation" => "FFHB",
        "url" => "http://www.ff-handball.org/",
        "logo" => "handball/ffhb.png",
        "logo_black" => "handball/ffhb_black.png",
    ];

    protected $ligues = [
        "lidl" => [
            "infos" => [
                "name" => "Lidl Starligue",
                "abbreviation" => "",
                "url" => "http://www.lnh.fr/lidl-starligue/accueil",
                "logo" => "handball/starligue.png",
                "sexe" => "M",
                "level" => 1,
            ],
            "teams" => [],
        ],
        "proligue" => [
            "infos" => [
                "name" => "Proligue",
                "abbreviation" => "",
                "url" => "http://www.lnh.fr/proligue/accueil",
                "logo" => "handball/proligue.png",
                "sexe" => "M",
                "level" => 2,

            ],
            "teams" => [],
        ]
    ];
}
