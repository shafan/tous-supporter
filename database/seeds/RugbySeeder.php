<?php

class RugbySeeder extends SportSeeder
{
    protected $infos_path = [
        'json_path' => 'rugby/teams.json',
        'logo_path' => 'rugby/logo/',
    ];
    
    protected $federation_infos = [
        "name" => "Fédération Française de Rugby",
        "abbreviation" => "FFR",
        "url" => "http://www.ffr.fr/",
        "logo" => "rugby/ffr.png",
        "logo_black" => "rugby/ffr_black.png",
    ];
    
    protected $ligues = [
        "top14" => [
            "infos" => [
                "name" => "TOP 14",
                "abbreviation" => "",
                "url" => "https://www.lnr.fr/rugby-top-14",
                "logo" => "rugby/top14.jpg",
                "sexe" => "M",
                "level" => 1,

            ],
            "teams" => [],
        ],
        "proD2" => [
            "infos" => [
                "name" => "PRO D2",
                "abbreviation" => "",
                "url" => "https://www.lnr.fr/rugby-pro-d2",
                "logo" => "rugby/prod2.jpg",
                "sexe" => "M",
                "level" => 2,

            ],
            "teams" => [],
        ]
    ];
}
