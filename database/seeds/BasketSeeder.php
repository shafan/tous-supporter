<?php

class BasketSeeder extends SportSeeder
{
    protected $infos_path = [
        'json_path' => 'basket/teams.json',
        'logo_path' => 'basket/logo/',
    ];

    protected $federation_infos = [
        "name" => "Fédération Française de BasketBall",
        "abbreviation" => "FFBB",
        "url" => "http://www.ffbb.com",
        "logo" => "basket/ffbb.png",
        "logo_black" => "basket/ffbb_black.png",
    ];

    protected $ligues = [
        "jeep_elite" => [
            "infos" => [
                "name" => "Jeep® ÉLITE",
                "abbreviation" => "",
                "url" => "https://www.lnb.fr/fr/pro-a/classement-14.html",
                "logo" => "basket/jeep_elite.png",
                "sexe" => "M",
                "level" => 1,
            ],
            "teams" => [],
        ],
        "pro_b" => [
            "infos" => [
                "name" => "Pro B",
                "abbreviation" => "",
                "url" => "https://www.lnb.fr/fr/pro-b/calendrier-prob-59.html",
                "logo" => "basket/pro_b.png",
                "sexe" => "M",
                "level" => 2,
            ],
            "teams" => [],
        ]
    ];
}
