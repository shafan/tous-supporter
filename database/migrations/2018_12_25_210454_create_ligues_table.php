<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ligues', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name');
            $table->string('abbreviation')->nullable();
            $table->string('url')->nullable();
            $table->boolean('validated')->default(false);
            $table->enum('sexe', ['M', 'F']);
            $table->unsignedSmallInteger('level');
            $table->string('logo_path');

            $table->unsignedSmallInteger('federation_id');

            $table->foreign('federation_id')
                  ->references('id')->on('federations')
                  ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ligues');
    }
}
