<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name');
            $table->string('fullname')->nullable();
            $table->string('abbreviation')->nullable();
            $table->string('ville')->nullable();
            $table->string('url')->nullable();
            $table->string('logo_path');
            $table->decimal('lon', 11, 8)->nullable();
            $table->decimal('lat', 10, 8)->nullable();
            $table->point('position')->nullable();
            $table->boolean('validated')->default(false);
            $table->string('bg_color')->default("#ffffff");
            $table->string('color')->default("#000000");


            $table->unsignedSmallInteger('ligue_id');
            $table->foreign('ligue_id')
                  ->references('id')->on('ligues')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
