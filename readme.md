## About Installation
### Installation de Laravel
- `$ composer create-project --prefer-dist laravel/laravel blog`
- `$ cd blog`

### Installation de Homestead
- `$ composer require laravel/homestead --dev`
- `$ php vendor/bin/homestead make`
#### Changer les paramètres de .env
```
APP_NAME=my-teams
APP_ENV=local
APP_KEY=base64:lsXEPTBpp+Obb64bTbEwyXM1rl1dMv8PCZzPdQ9dBLI=
APP_DEBUG=true
APP_URL=//myteams.pg

LOG_CHANNEL=stack

DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=localhost
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```
#### Changer les paramètres de Homestead.yaml
```
ip: 192.168.10.10
memory: 2048
cpus: 1
provider: virtualbox
authorize: ~/.ssh/id_rsa.pub
keys:
    - ~/.ssh/id_rsa
folders:
    -
        map: /home/pierre/Developer/Development/my_teams
        to: /home/vagrant/code
sites:
    -
        map: myteams.pg
        to: /home/vagrant/code/public
databases:
    - homestead
backup: true
name: my-teams
hostname: my-teams
```
#### Changer les paramètres de after.sh pour postgis

```bash
#!/usr/bin/env bash

# Install PostGIS extension (PostgreSQL)
# Let's check if postgis is already installed...
dpkg -s postgis >/dev/null 2>&1
# ... if it is, then it assumed that so is postgresql-10-postgis package and that
# the postgis extension was already created in the database -> bail early
if [[ $? -ne 0 ]]; then
	# Taken from https://elliotwms.github.io/2016/homestead-postgis/
	sudo apt-get update
	sudo apt-get install -y \
		postgis \
		postgresql-10-postgis-2.4

	# Assign the correct values to these variables
	db_user=homestead
	db_pass=secret
	db_name=homestead

	# Use PGPASSWORD to login without password prompt
	# https://www.postgresql.org/docs/9.1/static/libpq-envars.html
	PGPASSWORD=secret psql \
		-U $db_user \
		-h localhost \
		-c "CREATE EXTENSION postgis;" \
		$db_name
fi
```

### Mettre en place Vue.js avec Vue UI
#### remove existing frontend scaffold
`rm -rf package.json webpack.mix.js yarn.lock resources/js resources/lang resources/sass`

#### créer le projet vu
Soit avec vue
`vue create frontend`
voir directement avec vue ui
`vue ui`

#### Configure Vue project
Create `frontend/vue.config.js`

```
module.exports = {
  // proxy API requests to Valet during development
  devServer: {
    proxy: 'http//myteams.pg'
  },

  // output built static files to Laravel's public dir.
  // note the "build" script in package.json needs to be modified as well.
  outputDir: '../public',

  // modify the location of the generated HTML file.
  // make sure to do this only in production.
  indexPath: process.env.NODE_ENV === 'production'
    ? '../resources/views/index.blade.php'
    : 'index.html'
}
```

Edit frontend/package.json:
```
"scripts": {
  "serve": "vue-cli-service serve",
- "build": "vue-cli-service build",
+ "build": "rm -rf ../public/{js,css,img} && vue-cli-service build --no-clean",
  "lint": "vue-cli-service lint"
},
```

Pour que toutes les pages ailles vers vue.js, editer routes/web.php :

```
Route::get('/{any}', 'SpaController@index')->where('any', '.*');
```
On créé le COntroller : app/Http/Controllers/SpaController.php
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SpaController extends Controller
{
    public function index()
    {
        return view('index');
    }
}
```

### Lancer l'application
#### lancer vagrant pour le backend laravel
`vagrant up`

Si on veut entrer en ssh pour lancer les commandes php artisan

`vagrant ssh`

Puis ont lance la migration avec les seeders

`php artisan migrate --seed`

#### générer les clé auth
`php artisan passport:install`

on récupère le password grant client secret et on le met dans .env PASSPORT_CLIENT_SECRET

#### lancer vue 
on peut tout faire depuis vue ui"


### Divers
Pour débuger : utiliser [Telescope](http://myteams.pg/telescope)

Pour tester l'API : application Insomnia
